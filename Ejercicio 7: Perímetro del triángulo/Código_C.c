#include<stdio.h>

int main() {
	float a;
	float b;
	float c;
	float d;
	int op;
	do {
		printf("\n"); /* no hay forma directa de borrar la pantalla con C estandar */
		printf("Elegir el tipo de tri�ngulo\n");
		printf("   1. Equil�tero\n");
		printf("   2. Is�sceles\n");
		printf("   3. Escaleno\n");
		printf("   4. Salir\n");
		printf("Elija una opci�n (1-4): \n");
		scanf("%i",&op);
		switch (op) {
		case 1:
			printf("El tri�ngulo equil�tero es un pol�gono regular, teniendo sus tres lados iguales\n");
			getchar(); /* a diferencia del pseudoc�digo, espera un Enter, no cualquier tecla */
			printf("El lado del tri�ngulo equil�tero que se calcula el per�metro es de: \n");
			scanf("%f",&a);
			b = 3*a;
			printf("El per�metro del tri�ngulo es: %f\n",b);
			break;
		case 2:
			printf("El tri�ngulo is�sceles es aquel que tiene dos lados iguales y uno diferente\n");
			getchar(); /* a diferencia del pseudoc�digo, espera un Enter, no cualquier tecla */
			printf("La base del tri�ngulo es: \n");
			scanf("%f",&a);
			printf("El tama�o de los lados restantes es: \n");
			scanf("%f",&b);
			c = (2*b)+a;
			printf("El per�metro del tri�ngulo is�sceles es: %f\n",c);
			break;
		case 3:
			printf("El tri�ngulo escaleno es aquel que tiene sus tres lados diferentes\n");
			getchar(); /* a diferencia del pseudoc�digo, espera un Enter, no cualquier tecla */
			printf("El primer lado del tri�ngulo es: \n");
			scanf("%f",&a);
			printf("El segundo lado del tri�ngulo es: \n");
			scanf("%f",&b);
			printf("El tercer lado del tri�ngulo es: \n");
			scanf("%f",&c);
			d = a+b+c;
			printf("El per�metro del tri�ngulo escaleno es: %f\n",d);
			break;
		case 4:
			printf("Gracias\n");
			break;
		default:
			printf("Opci�n no v�lida\n");
		}
		printf("Presione enter para continuar\n");
		getchar(); /* a diferencia del pseudoc�digo, espera un Enter, no cualquier tecla */
	} while (op!=4);
	return 0;
}

