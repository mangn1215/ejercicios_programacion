Algoritmo Perimtero_triangulo
	Repetir
		Borrar Pantalla
		Escribir 'Elegir el tipo de tri�ngulo'
		Escribir '   1. Equil�tero'
		Escribir '   2. Is�sceles'
		Escribir '   3. Escaleno'
		Escribir '   4. Salir'
		Escribir 'Elija una opci�n (1-4): '
		Leer OP
		Segun OP  Hacer
			1:
				Definir a,b Como Real
				Escribir 'El tri�ngulo equil�tero es un pol�gono regular, teniendo sus tres lados iguales'
				Esperar Tecla
				Escribir 'El lado del tri�ngulo equil�tero que se calcula el per�metro es de: '
				Leer a
				b <- 3*a
				Escribir 'El per�metro del tri�ngulo es: ',b
			2:
				Definir a,b,c Como Real
				Escribir 'El tri�ngulo is�sceles es aquel que tiene dos lados iguales y uno diferente'
				Esperar Tecla
				Escribir 'La base del tri�ngulo es: '
				Leer a
				Escribir 'El tama�o de los lados restantes es: '
				Leer b
				c <- (2*b)+a
				Escribir 'El per�metro del tri�ngulo is�sceles es: ',c
			3:
				Definir a,b,c,d Como Real
				Escribir 'El tri�ngulo escaleno es aquel que tiene sus tres lados diferentes'
				Esperar Tecla
				Escribir 'El primer lado del tri�ngulo es: '
				Leer a
				Escribir 'El segundo lado del tri�ngulo es: '
				Leer b
				Escribir 'El tercer lado del tri�ngulo es: '
				Leer c
				d <- a+b+c
				Escribir 'El per�metro del tri�ngulo escaleno es: ',d
			4:
				Escribir 'Gracias'
			De Otro Modo:
				Escribir 'Opci�n no v�lida'
		FinSegun
		Escribir 'Presione enter para continuar'
		Esperar Tecla
	Hasta Que OP=4
FinAlgoritmo
