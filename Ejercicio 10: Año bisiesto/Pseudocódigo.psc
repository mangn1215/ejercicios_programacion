Algoritmo bisiesto
	Definir a Como Entero
	Escribir 'Anota un n�mero mayor al a�o 1582'
	Leer a
	Si a>=1582 Entonces
		Para x<-1582 Hasta a Con Paso 4 Hacer
			Si a=x Entonces
				Escribir 'El a�o: ',x,' es bisiesto'
			SiNo
				Escribir 'El a�o: ',a,' no es bisiesto'
			FinSi
		FinPara
	SiNo
		Escribir 'El n�mero debe ser mayor a 1582'
	FinSi
FinAlgoritmo
