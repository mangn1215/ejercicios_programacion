Algoritmo Desigualdad_triangulo
	Definir a, b, c Como Real
	Escribir "La longitud del primer segmento es: "
	Leer a
	Escribir "La longitud del segundo segmento es: "
	Leer b
	Escribir "La longitud del tercer segmento es: "
	Leer c
	Si (a=b y b=c) y (a=c y b=c) y (a=b y a=c) Entonces
		Escribir "Se puede formar un tri�ngulo Equil�tero, teniendo tres lados iguales"
	Sino 
		Si (a<>b y b<>c) y (a<>c y b<>c) y (a<>b y a<>c) Entonces
			Escribir "Se puede formar un tri�ngulo Escaleno, teniendo tres lados diferentes"
		Sino
			Escribir "Se puede formar un tri�ngulo Is�sceles, teniendo dos lados iguales"
		FinSi
	FinSi
FinAlgoritmo
