Algoritmo Ping_Pong
	Definir x Como Entero
	Para x<-1 Hasta 100 Hacer
		Si x MOD 3==0 Entonces
			Escribir ' ',x,' ping'
		FinSi
		Si x MOD 5==0 Entonces
			Escribir ' ',x,' pong'
		FinSi
		Si (x MOD 3==0) Y (x MOD 5==0) Entonces
			Escribir ' ',x,' ping-pong'
		FinSi
	FinPara
FinAlgoritmo
