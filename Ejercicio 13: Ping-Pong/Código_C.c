#include<stdio.h>

int main() {
	int x;
	for (x=1;x<=100;x+=1) {
		if (x%3==0) {
			printf(" %i ping\n",x);
		}
		if (x%5==0) {
			printf(" %i pong\n",x);
		}
		if ((x%3==0) && (x%5==0)) {
			printf(" %i ping-pong\n",x);
		}
	}
	return 0;
}

