Algoritmo Perimetro_T_Escaleno
	Definir a,b,c,d Como Real
	Escribir "El triángulo escaleno es aquel que tiene sus tres lados diferentes"
	Esperar Tecla
	Escribir "El primer lado del triángulo es: "
	Leer a
	Escribir "El segundo lado del triángulo es: "
	Leer b
	Escribir "El tercer lado del triángulo es: "
	Leer c
	d = a+b+c
	Escribir 'El perímetro del triángulo escaleno es: ', d
FinAlgoritmo
