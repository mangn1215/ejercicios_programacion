Los números pares son aquellos número que al ser dividios entre dos su residuo debe ser cero.

Lo primero que se hace es colocar una variable al número que se determinará si es par o impar.
La segunda variable que se determina es con la que va comenzar la búsqueda de los pares, es decir el número 1.
En tercer lugar se realiza el ciclo para, el cual inicia de 1 hasta 100, incrementando de 1 en 1 hasta 100.
Posterior se realiza la secuencia de acciones para determinar solo los número pares que se encuentran entre 1 hasta 100.
Dentro del ciclo for se realiza una condición if, apoyado de determinar el residuo (mod), para saber si es par.
