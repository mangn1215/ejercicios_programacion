Algoritmo Meses_ano
	Definir a Como Entero
	Escribir 'Escribe un n�mero entre 0 y 11'
	Leer a
	Si a>=0 Y a<=11 Entonces
		Si a=0 Entonces
			Escribir 'El mes elegido es Enero'
		FinSi
		Si a=1 Entonces
			Escribir 'El mes elegido es Febrero'
		FinSi
		Si a=2 Entonces
			Escribir 'El mes elegido es Marzo'
		FinSi
		Si a=3 Entonces
			Escribir 'El mes elegido es Abril'
		FinSi
		Si a=4 Entonces
			Escribir 'El mes elegido es Mayo'
		FinSi
		Si a=5 Entonces
			Escribir 'El mes elegido es Junio'
		FinSi
		Si a=6 Entonces
			Escribir 'El mes elegido es Julio'
		FinSi
		Si a=7 Entonces
			Escribir 'El mes elegido es Agosto'
		FinSi
		Si a=8 Entonces
			Escribir 'El mes elegido es Septiembre'
		FinSi
		Si a=9 Entonces
			Escribir 'El mes elegido es Octubre'
		FinSi
		Si a=10 Entonces
			Escribir 'El mes elegido es Noviembre'
		FinSi
		Si a=11 Entonces
			Escribir 'El mes elegido es Diciembre'
		FinSi
	SiNo
		Escribir 'El n�mero debe estar entre 0 y 11'
	FinSi
FinAlgoritmo
